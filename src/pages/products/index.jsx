import React, { Component } from "react";
import Navbar from "../../components/navbar";
import Cards from "./components/cards";
import Banner from "./components/banner";
import cardsData from "../../data/product.json";
import PopUp from "../../components/popup";

class Product extends Component {
  state = {
    webName: this.props.webName,
    cardsData: cardsData,
  };

  handelDalete = (cardsId) => {
    // const data = [];
    // for (let i = 0; i < this.state.cardsData.length; i++) {
    //   if (this.state.cardsData[i].id !== cardsId) {
    //     data.push(this.state.cardsData[i]);
    //     // delete this.state.cardsData[i];
    //   }
    // }
    // this.setState({ cardsData: data });
    // console.log(cardsId);
    // // delete this.state.cardsData[cardId];
    // console.log(this.state.cardsData);
    const data = this.state.cardsData.filter((c) => c.id !== cardsId);
    console.log(data);
    this.setState({ cardsData: data });
  };

  render() {
    return (
      <div className="container">
        <div className="text-center">
          <Banner name="Welcome To cards" />
        </div>
        {this.state.cardsData.map((cards) => (
          <PopUp
            id={cards.id}
            title={cards.title}
            imgSrc={cards.thumbnail}
            imgAlt={cards.imgAlt}
          />
        ))}
        <div className="row row-cols-1 row-cols-md-4 g-4">
          {console.log(this.state.cardsData)}
          {this.state.cardsData.map((cards) => (
            <Cards
              id={cards.id}
              title={cards.title}
              description={cards.description}
              imgSrc={cards.thumbnail}
              imgAlt={cards.imgAlt}
              btnText={cards.btnText}
              btnHref={cards.btnHref}
              onDelete={this.handelDalete}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default Product;
