import React, { Component } from "react";
// import { Button } from "antd";

class People extends Component {
  state = {
    default: "Hallo Semua",
  };

  handelClickMessage = (message) => {
    this.setState({ default: message });
    console.log(this.setState({ default: message }));
  };

  render() {
    return (
      <div className="cointainer">
        <div className="text-center">
          <h5>{this.state.default}</h5>
        </div>
        <div className="text-center">
          <div className="row">
            <div className="offset-5 col-1">
              <button
                class="btn btn-danger"
                onClick={() => this.handelClickMessage("Hallo Imam Ganteng")}
              >
                Imam
              </button>
            </div>
            <div className="col-1">
              <button
                class="btn btn-warning"
                onClick={() => this.handelClickMessage("Hallo Ariawan Keren")}
              >
                Ariawan
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default People;
