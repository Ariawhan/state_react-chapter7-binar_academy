import React, { Component } from "react";
import { Button } from "antd";

class Number extends Component {
  state = {
    number: 1,
  };

  handelClickCount = (count) => {
    this.setState({ number: this.state.number + count });
  };

  render() {
    return (
      <div className="cointainer">
        <div className="text-center">
          <h5>{this.state.number}</h5>
        </div>
        <div className="text-center">
          <Button
            type="primary"
            size="large"
            onClick={() => this.handelClickCount(1)}
          >
            increment
          </Button>
        </div>
      </div>
    );
  }
}

export default Number;
