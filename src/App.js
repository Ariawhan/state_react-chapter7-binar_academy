import Navbar from "./components/navbar";
import React from "react";
import "./App.css";
import Public from "./routes/public";
import { BrowserRouter } from "react-router-dom";
import Number from "./pages/btn-daily/number";
import People from "./pages/btn-daily/people";

function App() {
  const webName = "Ariawan Web";
  return (
    // <>
    //   <BrowserRouter>
    //     <div className="App">
    //       <main className="container">
    //         <Navbar webName={webName}></Navbar>
    //         <Public />
    //       </main>
    //     </div>
    //   </BrowserRouter>
    // </>
    <div>
      <div className="mt-3">
        <People></People>
      </div>
      <div className="mt-4">
        <Number></Number>
      </div>
    </div>
  );
}

export default App;
